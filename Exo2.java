import java.util.Scanner;

public class Exo2 {

    public static void main(String[] arg) {

        Scanner number = new Scanner(System.in);
        System.out.println("Veuillez saisir un nombre entre 10 et 20");
        int result = number.nextInt();
        if (result < 10) {
            System.out.println("Ceci est trop petit");

        } else if (result > 20) {
            System.out.println("Ce nombre est trop grand");

        } else {
            System.out.println("Bravo");
            if (result % 2 == 0) {
                System.out.println(result + " est pair");
            } else {
                System.out.println(result + " est impair");
            }
        }
        number.close();
    }
}
