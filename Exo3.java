
/**
 * Exo3
 */
import java.util.*;

public class Exo3 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Entrer une lettre :");
        char ch = sc.next().charAt(0);
        if (voyelleConsonne(ch) == true) {
            System.out.println("Ceci est une voyelle");
        } else {
            System.out.println("Ceci est une consonne");
        }

        System.out.println("Entrer une phrase :");
        char[] tabC;
        String msgUser = sc.next();
        tabC = msgUser.toCharArray();
        char[] tabD = new char[12];
        int y = 0;
        for (int i = 0; i < tabC.length; i++) {
            if (voyelleConsonne(tabC[i])) {
                tabD[y] = tabC[i];
                y++;
            }
        }
        System.out.println(tabD);
        System.out.println("Ceci est une voyelle");

        sc.close();

    }

    public static boolean voyelleConsonne(char ch) {
        if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == 'y') {
            return true;
        } else {
            return false;
        }

    }

}