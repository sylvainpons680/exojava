import java.util.*;

public class Livre {
    private String titre;
    private boolean dispo;
    private String auteur;
    private String style;
    private String format;
    private Lecteur emprunteur;

    public Livre(String unTitre, boolean uneDispo, String unAuteur, String unStyle, String unFormat,
            Lecteur nemprunteur) {
        this.titre = unTitre;
        this.dispo = uneDispo;
        this.auteur = unAuteur;
        this.style = unStyle;
        this.format = unFormat;
        this.emprunteur = unemprunteur;

    }

    void setTitre() {
        titre = "bouquin";
    }

    public String GetTitre() {

        return titre;
    }

    public boolean GetDispo() {
        return dispo;
    }

    public String GetAuteur() {
        return auteur;
    }

    public String GetStyle() {
        return style;
    }

    public String GetFormat() {
        return format;
    }

    public Lecteur Getemprunteur() {
        return emprunteur;
    }

    Date emprunte(Lecteur lec) {
        if (emprunteur == null) {
            emprunteur = lec;
            return new Date();
        } else
            return null;
    }

}
