import java.util.*;

public class Bibliothèque {

    private List<Livre> livres;
    private String name;

    public Bibliothèque(String name) {
        this.livres = new ArrayList<Livre>();
        this.name = name;
    }

    public void addBook(String unTitre, boolean uneDispo, String unAuteur, String unStyle, String unFormat) {
        Livre newBook = new Livre(unTitre, uneDispo, unAuteur, unStyle, unFormat, unFormat);
        livres.add(newBook);

    }

    public void afficherLivre() {

    }
}